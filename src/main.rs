mod word_reader;
mod word_counter;

use std::collections::{BTreeMap, HashMap};
use std::env;
use std::time::{Duration, Instant};
use crate::word_reader::WordReader;
use crate::word_counter::{Word, WordCounter};

fn main() {
    println!();
    println!();
    println!("WORD COUNTER in RUST");
    let args: Vec<String> = env::args().collect();
    let mut _file_name = "";
    let mut _print = "";
    if args.len() >= 2 {
        println!("running word count with file {:?}", args[1]);
        _file_name = &args[1];
        //_print = &args[2];
    }
    else {
        println!("running word count with file data/Text.txt");
        _file_name = "data/Text.txt";
    }
    let d1 = main_with_insert(_file_name);
    //let d1 = 0;
    let d2 = main_with_binary_insert(_file_name);
    let d3 = main_with_hashmap_unsorted(_file_name);
    let d4 = main_with_hashmap_sorted(_file_name);
    let d5 = main_with_hashmap2_unsorted(_file_name);
    let d6 = main_with_hashmap2_sorted(_file_name);
    let d7 = main_with_hashmap_sorted_with_treemap(_file_name);
    let d8 = main_with_treemap(_file_name);
    println!("RESULTS");
    println!("Time elapsed in count with insert is: {:?}", d1);
    println!("Time elapsed in count with binary insert is: {:?}", d2);
    println!("Time elapsed in count with hashmap unsorted is: {:?}", d3);
    println!("Time elapsed in count with hashmap sorted is: {:?}", d4);
    println!("Time elapsed in count with hashmap with entry unsorted is: {:?}", d5);
    println!("Time elapsed in count with hashmap with entry sorted is: {:?}", d6);
    println!("Time elapsed in count with hashmap sorted with treemap is: {:?}", d7);
    println!("Time elapsed in count with treemap is: {:?}", d8);
}
#[allow(dead_code)]
fn main_with_insert(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut wc = WordCounter::new();
    //let mut i = 0;
    let start = Instant::now();
    while let Some(word) = wr.next() {
        //println!("{} : {}", i, word);
        //i += 1;
        wc.insert(word);
    }
    let duration = start.elapsed();
    // println!("{}", wc.to_string());
    //println!("Time elapsed in count with insert is: {:?}", duration);
    duration
}

fn main_with_binary_insert(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut wc = WordCounter::new();
    //let mut i = 0;
    let start = Instant::now();
    while let Some(word) = wr.next() {
        //println!("{} : {}", i, word);
        //i += 1;
        wc.binary_insert(word);
    }
    let duration = start.elapsed();
    //println!("{}", wc.to_string());
    //println!("Time elapsed in count with binary insert is: {:?}", duration);
    duration
}

fn main_with_hashmap_unsorted(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut hash_map : HashMap<String, i64> = HashMap::new();
    let start = Instant::now();
    while let Some(word) = wr.next() {
        match hash_map.get(&word) {
            Some(n) => hash_map.insert(word, *n + 1),
            None => hash_map.insert(word, 1),
        };
    }
    let duration = start.elapsed();
    duration
}

fn main_with_hashmap_sorted(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut wc = WordCounter::new();
    let mut hash_map : HashMap<String, usize> = HashMap::new();
    let start = Instant::now();
    while let Some(word) = wr.next() {
        match hash_map.get(&word) {
            Some(n) => hash_map.insert(word, *n + 1),
            None => hash_map.insert(word, 1),
        };
    }
    for (word,n) in hash_map {
        wc.add(Word::new(word, n));
    }
    wc.sort();
    let duration = start.elapsed();
    duration
}

fn main_with_hashmap2_unsorted(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut hash_map : HashMap<String, usize> = HashMap::new();
    let start = Instant::now();
    while let Some(word) = wr.next() {
        hash_map.entry(word).and_modify(|n| *n += 1).or_insert(1);
    }
    let duration = start.elapsed();
    //println!("{}", wc.to_string());
    duration
}

fn main_with_hashmap2_sorted(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut wc = WordCounter::new();
    let mut hash_map : HashMap<String, usize> = HashMap::new();
    let start = Instant::now();
    while let Some(word) = wr.next() {
        hash_map.entry(word).and_modify(|n| *n += 1).or_insert(1);
    }
    for (word,n) in hash_map {
        wc.add(Word::new(word, n));
    }
    wc.sort();
    let duration = start.elapsed();
    //println!("{}", wc.to_string());
    duration
}

fn main_with_hashmap_sorted_with_treemap(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut hash_map : HashMap<String, i64> = HashMap::new();
    //let mut i = 0;
    let start = Instant::now();
    while let Some(word) = wr.next() {
        //println!("{} : {}", i, word);
        //i += 1;
        match hash_map.get(&word) {
            Some(n) => hash_map.insert(word, *n + 1),
            None => hash_map.insert(word, 1),
        };
    }
    let _btree_map: BTreeMap<String,i64> = hash_map.iter().into_iter().map(|(k,v)| (k.clone().to_string(),*v)).collect();
    let duration = start.elapsed();
    //println!("{:?}", _btree_map);
    //println!("Time elapsed in count with binary insert is: {:?}", duration);
    duration
}

fn main_with_treemap(_file_name: &str) -> Duration {
    let mut wr = WordReader::new(_file_name);
    let mut tree_map: BTreeMap<String, i64> = BTreeMap::new();
    //let mut i = 0;
    let start = Instant::now();
    while let Some(word) = wr.next() {
        //println!("{} : {}", i, word);
        //i += 1;
        match tree_map.get(&word) {
            Some(n) => tree_map.insert(word, *n + 1),
            None => tree_map.insert(word, 1),
        };
    }
    let duration = start.elapsed();
    //println!("{:?}", tree_map);
    //println!("Time elapsed in count with binary insert is: {:?}", duration);
    duration
}

