use std::ops::Add;

#[derive(PartialEq, PartialOrd, Eq, Ord)]
pub struct Word {
    word: String,
    n: usize,
}

impl Word {
    pub fn new(word : String, n :usize) -> Self {
        Word {
            word,
            n,
        }
    }
}

pub struct WordCounter {
    words: Vec<Word>
}

impl WordCounter {
    pub fn new() -> WordCounter {
        WordCounter {
            //words: Vec::with_capacity(50000),
            words: Vec::new(),
        }
    }

    pub fn add(&mut self, word : Word) {
        self.words.push(word)
    }

    pub fn sort(&mut self) {
        self.words.sort();
    }

    pub fn insert(&mut self, word: String) {
        let mut i = 0;
        while i < self.words.len() && word > self.words[i].word {
            i += 1;
        }
        if i < self.words.len() && word == self.words[i].word {
            self.words[i].n += 1;
        }
        else {
            self.words.insert(i, Word {word, n: 1});
        }
    }

    pub fn binary_insert(&mut self, word: String) {
        let mut left = 0;
        let mut right = self.words.len();
        let mut pos = (right + left) / 2;
        while right - left > 1  {
            if word >= self.words[pos].word {
                left = pos;
            }
            else {
                right = pos;
            }
            pos = (right + left) / 2
        }
        if self.words.len() > 0 {
            if word == self.words[pos].word {
                self.words[pos].n += 1;
                return
            } else if word > self.words[pos].word {
                pos += 1;
            }
        }
        self.words.insert(pos, Word {word, n: 1});
    }

    #[allow(dead_code)]
    pub fn to_string(&self) -> String {
        let mut out = String::new();
        for (i,s) in self.words.iter().enumerate() {
            out = out.add(&i.to_string());
            out = out.add(&":  ");
            out = out.add(&s.word);
            out = out.add(&":  ");
            out = out.add(&s.n.to_string());
            out = out.add(&"\n");
        }
        out
    }
}

#[cfg(test)]

#[test]
fn test_insert() {
    println!("WORD COUNTER insert");
    let mut wc = WordCounter::new();
    wc.insert("Hallo".to_string());
    wc.insert("Welt".to_string());
    wc.insert("Hallo".to_string());
    println!("{}", wc.to_string());
}

#[test]
fn test_binary_insert() {
    println!("WORD COUNTER binary insert");
    let mut wc = WordCounter::new();
    wc.binary_insert("Hallo".to_string());
    wc.binary_insert("Welt".to_string());
    wc.binary_insert("Hallo".to_string());
    println!("{}", wc.to_string());
}
