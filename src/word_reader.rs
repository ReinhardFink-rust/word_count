use std::fs;

pub struct WordReader {
    words: Vec<String>,
    cursor : usize,
}

impl WordReader {
    pub fn new(filename: &str) -> Self {
        let mut file_content = fs::read_to_string(filename).expect("Could not read file!");
        file_content.make_ascii_lowercase();
        //let pattern = [' ', '\n', '\t', '?', '!', '.', ',', ';', ':', '(', ')', '»', '«'];
        //let pattern = [' ', '\n', '\t', '?', '!', '.', ',', ';', ':', '(', ')','#'];
        let pattern = [' ', '\n', '\t', '?', '!', '.', ',', ';', ':', '(', ')'];
        WordReader {
            words : file_content.split(&pattern)
                .filter(|str| !str.is_empty())
                .map(|str| str.to_string())
                .collect(),
            cursor : 0,
        }
    }
}

impl Iterator for WordReader {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        //println!("{}",self.word_iter);
        let word = self.words.get(self.cursor).cloned();
        self.cursor += 1;
        word
    }
}
/*
HINT: IntoIterator is slower than Iterator
impl IntoIterator for WordReader {
    type Item = String;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.words.into_iter()
    }
}
*/
#[cfg(test)]
#[test]

fn test_word_reader() {
    println!("WORD READER");
    let mut wr = WordReader::new("data/Text.txt");
    let mut i = 0;
    while let Some(word) = wr.next() {
        println!("{} : {}", i, word);
        i += 1;
    }
}
