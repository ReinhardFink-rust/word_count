# Usage:
# make        # compile, run and delete word_count.out
CC = gcc
CFLAGS = -O3
# CFLAGS += -I /usr/include/glib-2.0
CFLAGS += `pkg-config --cflags glib-2.0`
LDFLAGS = `pkg-config --libs glib-2.0`
# gcc `pkg-config --cflags glib-2.0` foo.c `pkg-config --libs glib-2.0`

ifeq ("$(wildcard $(/home/optProgs/graalvm/))","")
    GRAALVMPATH = "/home/optProgs/graalvm"
else
    GRAALVMPATH = "/home/shares/optProgs/Informatik/graalvm"
endif

MAKEFLAGS += --silent

short _: INPUT_FILE= "./data/Text.txt"
medium _: INPUT_FILE= "./data/Monaden.txt"
long _: INPUT_FILE= "./data/Bible.txt"

short: run

medium: run

long: run

# delete: run
#	rm src_ansi_c/word_count.out

run: build
# C
	src_ansi_c/word_count.out ${INPUT_FILE}
# RUST
	target/release/count_words ${INPUT_FILE}
# JAVA
	java -classpath out/production Main ${INPUT_FILE}
#GRAALVM
	${GRAALVMPATH}/bin/java -classpath out/production_graalvm Main ${INPUT_FILE}
	out/production_graalvm/main ${INPUT_FILE}

build:
# C
	${CC} ${CFLAGS} src_ansi_c/main.c src_ansi_c/list.c src_ansi_c/vec.c -o src_ansi_c/word_count.out ${LDFLAGS}
# RUST
	cargo build --quiet --release
# JAVA
	javac -d out/production src_java/*java
# GRAALVM
	${GRAALVMPATH}/bin/javac -cp ${GRAALVMPATH} -d out/production_graalvm src_java/*java
	${GRAALVMPATH}/bin/native-image -O3 -march=native --gc=G1 -cp out/production_graalvm Main -o out/production_graalvm/main


