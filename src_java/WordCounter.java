import java.util.ArrayList;
import java.util.Collections;

class Word implements Comparable<Word>  {
    String word;
    int n;

    public Word(String word, int n) {
        this.word = word;
        this.n = n;
    }

    @Override
    public String toString() {
        return word + ": " + n;
    }

    @Override
    public int compareTo(Word other) {
        return this.word.compareTo(other.word);
    }
}
public class WordCounter {

    private final ArrayList<Word> words;
    public WordCounter() {
        this.words = new ArrayList<>();
    }

    public void add(String word, Integer n) {
        this.words.add(new Word(word, n));
    }

    public void sort() {
        Collections.sort(words);
    }

    public void insert_without_copy(String word) {
        int i = 0;
        while (i < words.size() && word.compareTo(words.get(i).word) > 0) {
            i += 1;
        }
        if (i < words.size() && word.equals(words.get(i).word)) {
            words.get(i).n += 1;
        }
        else {
            words.add(i, new Word(word,1));
        }
    }

    public void insert_with_copy(String word) {
        int i = 0;
        while (i < words.size() && word.compareTo(words.get(i).word) > 0) {
            i += 1;
        }
        if (i < words.size() && word.equals(words.get(i).word)) {
            words.get(i).n += 1;
        }
        else {
            words.add(i, new Word(new String(word),1));
        }
    }


    public void binary_insert(String word) {
        int left = 0;
        int right = words.size();
        int pos = (right + left) / 2;
        while (right - left > 1)  {
            if (word.compareTo(words.get(pos).word) >= 0) {
                left = pos;
            }
            else {
                right = pos;
            }
            pos = (right + left) / 2;
        }
        if (!words.isEmpty()) {
            if (word.equals(words.get(pos).word)) {
                words.get(pos).n += 1;
                return;
            } else if (word.compareTo(words.get(pos).word) > 0) {
                pos += 1;
            }
        }
        words.add(pos, new Word(word, 1));
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < words.size(); i++) {
            out.append(i).append(": ").append(words.get(i)).append("\n");
        }
        return out.toString();
    }
}
