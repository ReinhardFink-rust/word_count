import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        System.out.println();
        System.out.println();
        System.out.println("WORD COUNTER in JAVA");
        String fileName;
        if (args.length == 1) {
            System.out.println("running word count with file " + args[0]);
            fileName = args[0];
        }
        else {
            System.out.println("running word count with file ../../data/Text.txt");
            fileName = "data/Text.txt";
            //fileName = "data/Bible.txt";
        }
        long d1 = main_with_insert_without_copy(fileName);
        //long d1 = 0;
        long d2 = main_with_insert_with_copy(fileName);
        //long d2 = 0;
        long d3 = main_with_binary_insert(fileName);
        long d4 = main_with_hashmap_not_sorted(fileName);
        long d5 = main_with_hashmap_sorted(fileName);
        long d6 = main_with_hashmap_sorted_treemap(fileName);
        long d7 = main_with_treemap(fileName);
        System.out.println("RESULTS");
        System.out.println("Time elapsed in count with insert without copy is: " + (double)d1/1000000000 + "s");
        System.out.println("Time elapsed in count with insert with copy is: " + (double)d2/1000000000 + "s");
        System.out.println("Time elapsed in count with binary insert is: " + (double)d3/1000000 + "ms");
        System.out.println("Time elapsed in count with HashMap unsortet is: " + (double)d4/1000000 + "ms");
        System.out.println("Time elapsed in count with HashMap sortet is: " + (double)d5/1000000 + "ms");
        System.out.println("Time elapsed in count with HashMap sortet with TreeMap is: " + (double)d6/1000000 + "ms");
        System.out.println("Time elapsed in count with TreeMap is: " + (double)d7/1000000 + "ms");
    }
    private static long main_with_insert_without_copy(String fileName) {
        WordReader wr = new WordReader(fileName);
        WordCounter wc = new WordCounter();
        Instant start = Instant.now();
        while (wr.hasNext())  {
            wc.insert_without_copy(wr.next());
        }
        Instant finish = Instant.now();
        //System.out.println(wc);
        return Duration.between(start, finish).toNanos();
    }

    private static long main_with_insert_with_copy(String fileName) {
        WordReader wr = new WordReader(fileName);
        WordCounter wc = new WordCounter();
        Instant start = Instant.now();
        while (wr.hasNext())  {
            wc.insert_with_copy(wr.next());
        }
        Instant finish = Instant.now();
        //System.out.println(wc);
        return Duration.between(start, finish).toNanos();
    }

    private static long main_with_binary_insert(String fileName) {
        WordReader wr = new WordReader(fileName);
        WordCounter wc = new WordCounter();
        Instant start = Instant.now();
        while (wr.hasNext())  {
            wc.binary_insert(wr.next());
        }
        Instant finish = Instant.now();
        //System.out.println(wc);
        return Duration.between(start, finish).toNanos();
    }

    private static long main_with_hashmap_not_sorted(String fileName) {
        WordReader wr = new WordReader(fileName);
        WordCounter wc = new WordCounter();
        HashMap<String, Integer> map = new HashMap<>(wr.getLength(),(float)0.75);
        Instant start = Instant.now();
        while (wr.hasNext())  {
            String word = wr.next();
            Integer i = map.putIfAbsent(word, 1);
            if (i != null) {
                map.replace(word, i, i+1);
            }
        }
        map.forEach(wc::add);
        Instant finish = Instant.now();
        //System.out.println(wc);
        return Duration.between(start, finish).toNanos();
    }

    private static long main_with_hashmap_sorted(String fileName) {
        WordReader wr = new WordReader(fileName);
        WordCounter wc = new WordCounter();
        HashMap<String, Integer> map = new HashMap<>(wr.getLength(),(float)0.75);
        Instant start = Instant.now();
        while (wr.hasNext())  {
            String word = wr.next();
            Integer i = map.putIfAbsent(word, 1);
            if (i != null) {
                map.replace(word, i, i+1);
            }
        }
        map.forEach(wc::add);
        wc.sort();
        Instant finish = Instant.now();
        //System.out.println(wc);
        return Duration.between(start, finish).toNanos();
    }
    
    private static long main_with_hashmap_sorted_treemap(String fileName) {
        WordReader wr = new WordReader(fileName);
        HashMap<String, Integer> map = new HashMap<>(wr.getLength(),(float)0.75);
        Instant start = Instant.now();
        while (wr.hasNext())  {
            String word = wr.next();
            Integer i = map.putIfAbsent(word, 1);
            if (i != null) {
                map.replace(word, i, i+1);
            }
        }
        Map<String,Integer> wc = new TreeMap<String, Integer>();
        map.forEach(wc::put);
        Instant finish = Instant.now();
        //wc.forEach((word, count) -> { System.out.println(word+ ": "+count);});
        return Duration.between(start, finish).toNanos();
    }

    private static long main_with_treemap(String fileName) {
            WordReader wr = new WordReader(fileName);
            TreeMap<String, Integer> map = new TreeMap<>();
            Instant start = Instant.now();
            while (wr.hasNext())  {
                String word = wr.next();
                Integer i = map.putIfAbsent(word, 1);
                if (i != null) {
                    map.replace(word, i, i+1);
                }
            }
            Map<String,Integer> wc = new TreeMap<String, Integer>();
            map.forEach(wc::put);
            Instant finish = Instant.now();
            //wc.forEach((word, count) -> { System.out.println(word+ ": "+count);});
            return Duration.between(start, finish).toNanos();
        }



}
