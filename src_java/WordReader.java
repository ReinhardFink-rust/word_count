import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WordReader {
    private String[] strings;
    private int cursor;
    public WordReader(String fileName) {
        try {
            String content = Files.readString(Path.of(fileName)).toLowerCase();
            //strings = content.split("[ \\n\\t?!.,;:(),#]");
            strings = content.split("[ \\n\\t?!.,;:()]");
            this.cursor = 0;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getLength() {
        return strings.length;
    }

    public boolean hasNext() {
        return cursor < strings.length;
    }
    public String next() {
        while (strings[cursor].isEmpty() && hasNext()) {
            cursor++;
        }
        return strings[cursor++];
    }
}
