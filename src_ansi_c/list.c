#include "list.h"

void insert_into_list(List* list, char* word)
{
	//printf("process: %s\n", word);
	Node* current = list->head;
	Node* last = list->head;
	while (current != NULL && strcmp(word, current->word) > 0)
	{
		//printf("current = current->next\n");
		last = current;
		current = current->next;
	}
	if (current != NULL && strcmp(word, current->word) == 0)
	{
		//printf("current->count++\n");
		current->count++;
	}
	else
	{
		// create and fill new node
		//printf("create new node\n");
		Node* new_node = malloc(sizeof(Node));
		new_node->count = 1;
		new_node->word = malloc(strlen(word) * sizeof(char) + 1);
		strcpy(new_node->word,word);
		new_node->next = NULL;
		// insert node before
		// TODO: NOT ELEGANT
		if (current == list->head)
		{
			//printf("add first element\n");
			new_node->next = current;
			list->head = new_node;
		}
		else
		{
			//printf("add non first element\n");
			last->next = new_node;
			new_node->next = current;
		}
	}
}

void pretty_print_list(List* list)
{
	int column_size = 20;
	printf("Word			   Count\n");
	printf("------------------------\n");
	Node* current = list->head;
	while (current != NULL)
	{
		printf("%s:", current->word);
		for (int i = 0; i < column_size - strlen(current->word); i++)
			printf(" ");
		printf("%i\n", current->count);
		current = current->next;
	}
}

void print_list(List* list)
{
	int i = 0;
	Node* current = list->head;
	while (current != NULL)
	{
		printf("%i:  %s: %i\n", i++, current->word, current->count);
		current = current->next;
	}
}