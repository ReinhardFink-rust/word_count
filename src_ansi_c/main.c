#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <glib.h>

#include "list.h"
#include "vec.h"

int is_delimiter(char c) 
{
	return (c == ' ' ||
		c == '\n' ||
		c == '\t' ||
		c == '?' ||
		c == '!' ||
		c == '.' || 
		c == ',' ||
		c == ';' ||
		c == ':' ||
        //	c == '#' ||
		c == '(' || 
		c == ')');
}

char* next_word(FILE* file)
{
	char* buffer = malloc(100 * sizeof(char));
	int buffer_cursor = 0;
	char input_char; 
	while (1) {
		input_char = (char)fgetc(file);
		if (input_char == EOF)
		{
			//printf("EOF\n");
			return NULL;
		}
		else if (is_delimiter(input_char))
		{
			if (buffer_cursor > 0)
			{
				//printf("%c is not an alphabet.\n", c);
				// terminate with \0
				buffer[buffer_cursor++] = '\0';
				return buffer;
			}
		}
		else
		{
			//printf("%c is in an alphabet.\n", c);
			buffer[buffer_cursor++] = tolower(input_char);
		}
	}
}

int read_words_to_array(FILE* file, char** word_array)
{
	char* word = next_word(file);
	int i = 0;
	while (word != NULL)
	{
		word_array[i++] = word;
		word = next_word(file);
	}
	return i;
}


double main_word_from_file(FILE* file, List* list)
{
	printf("read words from file\n");
	char* word = next_word(file);
	clock_t t = clock();
	while (word != NULL)
	{
		//printf("%s\n", word);
		insert_into_list(list,word);
		word = next_word(file);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC;
	//print_list(list);
	//printf("runtime=%f\n", result);
	return result;
}

double main_word_from_array(FILE* file, List* list)
{
	printf("read words from array\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		insert_into_list(list,word_array[i]);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC;
	//print_list(list);
	//printf("runtime=%f\n", result);
	free(word_array);
	return result;
}

double main_word_from_vec_with_copy(FILE* file, Vec* vec)
{
	//printf("\nfrom VEC with copy\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		insert_into_vec_with_copy(vec,word_array[i]);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC;
	for (int i = 0; i < size; i++)
	{
		free(word_array[i]);
	}
	free(word_array);
	//print_vec(vec);
	//printf("runtime=%f\n", result);
	return result;
}

double main_word_from_vec_without_copy(FILE* file, Vec* vec)
{
	//printf("\nfrom VEC without copy\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		insert_into_vec_without_copy(vec,word_array[i]);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC;
	free(word_array);
	return result;
}

double main_word_from_vec_with_binary_insert(FILE* file, Vec* vec)
{
	//printf("\nfrom VEC with binary insert\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		bin_insert_into_vec(vec,word_array[i]);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC*1000;
	for (int i = 0; i < size; i++)
	{
		free(word_array[i]);
	}
	free(word_array);
	return result;
}

void print_hash_table_entry(gpointer key, gpointer value, gpointer user_data)
{
	printf("%s : %d\n", (char*)key, GPOINTER_TO_INT(value));
}

double main_with_g_hash_table_unsorted(FILE* file, Vec* vec)
{
	//printf("\nfrom VEC with binary insert\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	GHashTable* g_hash_table = g_hash_table_new (g_str_hash, g_str_equal);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		gpointer v = g_hash_table_lookup(g_hash_table, word_array[i]);
		//printf("%p", v);
		if (v == NULL)
		{
			v = GINT_TO_POINTER(1);
		}
		else
		{
			int n = GPOINTER_TO_INT(v);
			n++;
			v = GINT_TO_POINTER(n);
		}
		g_hash_table_insert(g_hash_table,word_array[i],v);
	}
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC*1000;
	return result;
}

void add_vec_element_to_array(gpointer key, gpointer value, gpointer user_data)
{
	VecElement* new_vec_element = malloc(sizeof(VecElement));
	new_vec_element->count = GPOINTER_TO_INT(value);
	new_vec_element->word = key;
	g_ptr_array_add((GPtrArray*) user_data, new_vec_element);
}

void print_array_element(gpointer data, gpointer user_data)
{
	printf("%s : %i\n", ((VecElement*) data)->word, ((VecElement*) data)->count);
}

static gint compare_vecs (gconstpointer a, gconstpointer b)
{
	  const VecElement* entry1 = *((VecElement**) a);
	  const VecElement* entry2 = *((VecElement**) b);
	  //return g_ascii_strcasecmp (entry1->word, entry2->word);
	  return strcmp (entry1->word, entry2->word);
}

double main_with_g_hash_table_sorted(FILE* file, Vec* vec)
{
	//printf("\nfrom VEC with binary insert\n");
	char** word_array = malloc(sizeof(char*) * 1000000);
	int size = read_words_to_array(file, word_array);
	GHashTable* g_hash_table = g_hash_table_new (g_str_hash, g_str_equal);
	double t = clock();
	for (int i = 0; i < size; i++)
	{
		gpointer v = g_hash_table_lookup(g_hash_table, word_array[i]);
		//printf("%p", v);
		if (v == NULL)
		{
			v = GINT_TO_POINTER(1);
		}
		else
		{
			int n = GPOINTER_TO_INT(v);
			n++;
			v = GINT_TO_POINTER(n);
		}
		g_hash_table_insert(g_hash_table,word_array[i],v);
	}
	//g_hash_table_foreach(g_hash_table, print_hash_table_entry, NULL);
	GPtrArray* array = g_ptr_array_new();
	g_hash_table_foreach(g_hash_table, add_vec_element_to_array, array);
	//g_ptr_array_foreach (array, print_array_element, NULL);
	g_ptr_array_sort(array, compare_vecs);
	double result = ((double)(clock() - t))/CLOCKS_PER_SEC*1000;
	//g_ptr_array_foreach(array, print_array_element, NULL);
	return result;
}



int main(int argc, char **argv)
{
	//char* fileName = "data/Bible.txt";
	//char* fileName = "data/Text.txt";
	printf("\n\n");
	printf("WORD COUNTER in C\n");
	char* fileName = argv[1];
	printf("running word count with file: %s\n", fileName);
	//printf("%s", fileName);
	FILE* file;
	List* list = malloc(sizeof(List));
	Vec* vec = malloc(sizeof(Vec));
	vec->size = 10;
	vec->words = malloc(vec->size * sizeof(VecElement*));
	vec->len = 0;

	// read from file to list
	file = fopen(fileName, "r");
	list->head = NULL;
	//double r1 = main_word_from_file(file, list);
	double r1 = 0;
	fclose(file);

	// read from array to list
	file = fopen(fileName, "r");
	list->head = NULL;
	double r2 = main_word_from_array(file, list);
	//double r2 = 0;
	fclose(file);
	//printf("from file: %f	   from array: %f\n", r1, r2);

	// read from array to vec with copy
	file = fopen(fileName, "r");
	double r3 = main_word_from_vec_with_copy(file, vec);
	//double r3 = 0;
	fclose(file);
	//printf("from file: %f	   from array: %f	   to vec: %f\n", r1, r2, r3);

	// read from array to vec without copy
	vec->size = 10;
	vec->words = malloc(vec->size * sizeof(VecElement*));
	vec->len = 0;
	file = fopen(fileName, "r");
	double r4 = main_word_from_vec_without_copy(file, vec);
	//double r4 = 0;
	fclose(file);

	// read from array with binary insert
	vec->size = 10;
	vec->words = malloc(vec->size * sizeof(VecElement*));
	vec->len = 0;
	file = fopen(fileName, "r");
	double r5 = main_word_from_vec_with_binary_insert(file, vec);
	//double r5 = 0;
	fclose(file);

	// read from array with binary insert
	vec->size = 10;
	vec->words = malloc(vec->size * sizeof(VecElement*));
	vec->len = 0;
	file = fopen(fileName, "r");
	double r6 = main_with_g_hash_table_unsorted(file, vec);
	//double r6 = 0;
	fclose(file);

	// read from array with binary insert
	vec->size = 10;
	vec->words = malloc(vec->size * sizeof(VecElement*));
	vec->len = 0;
	file = fopen(fileName, "r");
	double r7 = main_with_g_hash_table_sorted(file, vec);
	//double r7 = 0;
	fclose(file);
	printf("RESULTS\n");
	//printf("from file to list:   %f\n", r1);
	//printf("from array to list:  %f\n", r2);
	printf("Time elapsed in count with linear insert and strcp is:  %fs\n", r3);
	printf("Time elapsed in count with linear insert and pointer copy is: %fs\n", r4);
	printf("Time elapsed in count with binary insert is:  %fms\n", r5);
	printf("Time elapsed in count with g_hash_map unsorted is:  %fms\n", r6);
	printf("Time elapsed in count with g_hash_map sorted is:  %fms\n", r7);
}
