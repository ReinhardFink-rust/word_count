#include "vec.h"


void insert(int pos, VecElement* vec_element, Vec* vec)
{
	if (vec->len >= vec->size)
	{
		//printf("from %i to %i\n", vec->size , vec->size * 2);
		VecElement** new_words = malloc(2 * vec->size * sizeof(VecElement*));
		// copy front part
		for (int i = 0; i < pos; i++)
		{
			new_words[i] = vec->words[i];
		}
		// insert new element
		new_words[pos] = vec_element;
		// copy back part
		for (int i = pos; i < vec->len; i++)
		{
			new_words[i+1] = vec->words[i];
		}
		free(vec->words);
		vec->words = new_words;
		vec->len++;
		vec->size *= 2;
	}
	else
	{
		for (int i = vec->len; i > pos; i--)
		{
			vec->words[i] = vec->words[i-1];
		}
		vec->words[pos] = vec_element;
		vec->len++;
	}
}

void insert_into_vec_with_copy(Vec* vec, char* word)
{
	int pos = 0;
	while (pos < vec->len && strcmp(word, vec->words[pos]->word) > 0)
	{
		pos++;
	}
	if (pos < vec->len && strcmp(word, vec->words[pos]->word) == 0)
	{
		//printf("current->count++\n");
		vec->words[pos]->count++;
	}
	else
	{
		// create and fill new ListElement
		//printf("create new new_vec_element\n");
		VecElement* new_vec_element = malloc(sizeof(VecElement));
		new_vec_element->count = 1;
		new_vec_element->word = malloc(strlen(word) * sizeof(char) + 1);
		strcpy(new_vec_element->word,word);
		insert(pos, new_vec_element, vec);
	}
}

void insert_into_vec_without_copy(Vec* vec, char* word)
{
	int pos = 0;
	while (pos < vec->len && strcmp(word, vec->words[pos]->word) > 0)
	{
		pos++;
	}
	if (pos < vec->len && strcmp(word, vec->words[pos]->word) == 0)
	{
		//printf("current->count++\n");
		vec->words[pos]->count++;
	}
	else
	{
		// create and fill new ListElement
		//printf("create new new_vec_element\n");
		VecElement* new_vec_element = malloc(sizeof(VecElement));
		new_vec_element->count = 1;
		new_vec_element->word = word;
		insert(pos, new_vec_element, vec);
	}
}

void bin_insert_into_vec(Vec* vec, char* word)
{
	int left = 0;
	int right = vec->len;
	int pos = (right + left) / 2;
	while (right - left > 1)
	{
		if (strcmp(word, vec->words[pos]->word) >= 0)
		{
			left = pos;
		}
		else
		{
			right = pos;
		}
		pos = (right + left) / 2;
	}
	if (vec->len > 0)
	{
		if (strcmp(word, vec->words[pos]->word) == 0)
		{
			vec->words[pos]->count++;
			return;
		}
		else if (strcmp(word, vec->words[pos]->word) > 0)
		{
			pos++;
		}
	}
	VecElement* new_vec_element = malloc(sizeof(VecElement));
	new_vec_element->count = 1;
	//new_vec_element->word = word;
	new_vec_element->word = malloc(strlen(word) * sizeof(char) + 1);
	strcpy(new_vec_element->word,word);
	insert(pos, new_vec_element, vec);
	//printf("insert: %s at %i\n", word, pos);
	//print_vec(vec);
}


void print_vec(Vec* vec)
{
	for (int i = 0; i < vec->len; i++)
	{
		printf("%i:  %s: %i\n", i, vec->words[i]->word, vec->words[i]->count);
	}
}
