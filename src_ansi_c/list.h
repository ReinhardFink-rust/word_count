#ifndef __LIST_H__
#define __LIST_H__

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

typedef struct Node {
	int count;
	char* word;
	struct Node* next;
} Node;

typedef struct List {
	Node* head;
} List;


void insert_into_list(List* list, char* word);

void pretty_print_list(List* list);

void print_list(List* list);

#endif