#ifndef __VEC_H__
#define __VEC_H__

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

typedef struct VecElement {
	int count;
	char* word;
} VecElement;

typedef struct Vec {
	VecElement** words;
	int len;
	int size;
} Vec;

void insert(int pos, VecElement* vec_element, Vec* vec);

void insert_into_vec_with_copy(Vec* vec, char* word);

void insert_into_vec_without_copy(Vec* vec, char* word);

void bin_insert_into_vec(Vec* vec, char* word);

void print_vec(Vec* vec);


#endif